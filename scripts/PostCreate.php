<?php

class PostCreate {

    public static function writeTemplatedFile($src, $dst, $vars) {
        $contents = file_get_contents($src);
        $contents = str_replace(array_keys($vars), array_values($vars), $contents);
        file_put_contents($dst, $contents);
    }

    public static function ScaffoldFiles($event) {
        $project_dir = dirname(__DIR__);
        $domain = basename($project_dir);
        $vars = [
            '%DOMAIN%'  => $domain, 
        ];

        /**
         * Copy theme files into place
         */
        $src = "$project_dir/scaffold/theme";
        $dst = "$project_dir/pub/";
        rename($src, $dst);

        self::writeTemplatedFile("$dst/gulpfile.babel.js", "$dst/gulpfile.babel.js", $vars);
    }
}