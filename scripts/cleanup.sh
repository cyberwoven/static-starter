#!/bin/bash

echo "Deleting installation files..."
rm -rf scripts
rm -rf scaffold
rm -rf vendor
rm readme.md
rm default.packages.json
rm composer.*

echo "Creating local git repo..."
git init . -b master
git add .
git commit -m "post install"

URL="http://$(basename $(pwd)).test"
open $URL
